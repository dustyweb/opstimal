;; <paroneayea> hm
;; <paroneayea> so fluids/params defined outside a delimited continuation, but
;;              then set to a value within it, don't retain that value
;; <paroneayea> is this right?
;; <paroneayea> or rather
;; <paroneayea> they mutate externally
;; <paroneayea> and are dependent on whatever happens externally too
;; <paroneayea> eg
;; *** heroux (~heroux@gateway/shell/insomnia247/x-pdezwkxquifhayng) has joined
;;     channel #guile
;; <davexunit> paroneayea: they retain the value throughout the dynamic extent of
;;             the form that they are "parameterized" in
;; <paroneayea> davexunit: hm http://pamrel.lu/7b90a/
;; <paroneayea> I thought that would be true but my test above seems lik it
;;              isn't?
;; <davexunit> in Sly, I use a parameter called 'current-agenda', and the value
;;             is retained in my coroutines and such
;; <paroneayea> davexunit: that's what I want, but I don't seem to be getting
;;              that behavior.  Could you check whta I'm doing wrong?
;; <davexunit> paroneayea: use 'parameterize'
;; <paroneayea> davexunit: ohhhh
;; <paroneayea> I see
;; <paroneayea> davexunit: that makes a lot of sense :)
;; <mark_weaver> paroneayea: see
;;               http://debbugs.gnu.org/cgi/bugreport.cgi?bug=18356#17 for an
;;               explanation
;; <paroneayea> mark_weaver: davexunit: perfect, I now have the behavior I want
;;
;; <paroneayea> thank you!
;; <mark_weaver> you're welcome!
