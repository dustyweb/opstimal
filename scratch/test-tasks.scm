;; -----------------------------------------

(define *deps*
  (list "mysql-server"
        "mysql-client"
        "apache2"
        "apache2-doc"
        "openssl"
        "git"
        "php5"
        "php5-mysql"
        "libapache2-mod-php5"
        "postfix"))


(define *tasks*
  ;; In the future: (task-list-with-handlers)
  (task-list
   ;; ---------------
   ;; Update packages
   ;; ---------------
   (apt-get-update)

   (run-all
    (map
     (lambda (dep)
       (apt-get-install dep))
     *deps*)
    #:desc "Install all dependencies")

   ;; ---------------------------------
   ;; Copy application / apache configs
   ;; ---------------------------------
   (notify-if-changed
    (run-all
     #:desc "Copy over dbconnopen files"
     (map
      (copy-file-task
       (format #f "../files/dbconnopen_dev/~a.php" item)
       (format #f "/var/www/ttm/~a/include/dbconnopen.php" item)
       #:owner "www-data"
       #:group "www-data")
      (list "enlace"
            "lsna"
            "bickerdike"
            "swop"
            "trp")))
    ;; I think hy style notify things is wronnnnnng for guile.
    ;; this takes an alternate route where the parent function can look for
    ;; "notify" properties in its children.
    ;;
    ;; @@: Should this be generic enough so that all tasks know how to
    ;; notify w/ task-run and storing notifications on the task object?
    #:notify '(restart-apache))

   ;; #:notify-handlers
   ;; `((restart-apache . ,(service "apache" "restart")))
   ))


;; --------------------------------------
;; experiments
;; --------------------------------------

;; Huh

; (syntax-case stx ()
;   ((form docstring arg) (string? (syntax->datum #'docstring))
;    #'(form arg)) ((form arg) ...))


;; <paron_remote> does someone have an example I could look at on how to define
;;                syntax that *optionally* contains a docstring, or some such,
;;                but doesn't involve a lot of redefining the same code?
;;                                                                         [11:16]
;; <paron_remote> eg, I have
;; <paron_remote> http://pamrel.lu/5f824/                                  [11:24]
;; *** phant0mas (~quassel@147.95.122.136) has quit: Remote host closed the
;;     connection                                                          [11:27]
;; <wingo> usually you use syntax-case with an extra clause
;; <davexunit> beat me to it :)
;; <paron_remote> wingo: aha, ok!                                          [11:28]
;; <wingo> (syntax-case stx () ((form docstring arg) (string? (syntax->datum
;;         #'docstring)) #'(form arg)) ((form arg) ...))
;; <wingo> i think our syntax-rules might allow guard clauses now, i don't
;;         remember
;; <davexunit> they do                                                     [11:29]
;; <wingo> we should add (define-syntax-rules foo clause ...)              [11:30]
;; <wingo> plural rules
;; <davexunit> yeah, that would be nice.                                   [11:31]
;; <davexunit> here's an example of syntax-case macro with a guard clause:
;;             https://gitorious.org/sly/sly/source/a915b370094a59f868b6d49ab50e10c062600b46:sly/signal.scm#L158
;; <davexunit> warning: this macro probably sucks                          [11:32]
;; <davexunit> but it works :)
