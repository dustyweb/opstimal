(define-module (userops remote)
  #:use-module (userops core)
  #:use-module (oop goops)
  #:export (shell-quote
            run-command-remotely))


;; This function borrowed from guix
(define (shell-quote str)
  ;; Sort-of shell-quote STR so it can be passed as an argument to the
  ;; shell.
  (with-output-to-string
    (lambda ()
      (write str))))

(define (run-command-remotely host command)
  (apply system*
         "ssh" "-p"
         (number->string (slot-ref host 'port))
         (slot-ref host 'host)
         (map shell-quote command)))
