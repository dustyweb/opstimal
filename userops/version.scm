(define-module (userops version)
  #:export (userops-version-number))

(define userops-version-number "0.1.dev")
