(define-module (userops cli)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (userops version)
  #:use-module (userops remote)
  #:use-module (userops core)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:export (make-subcommand
            <subcommand>
            subcommand?
            subcommand-name
            subcommand-help
            subcommand-runs
            show-help
            show-version
            cli-builder
            get-host-from-list
            standard-cli
            common-subcommands
            ))

(define-record-type <subcommand>
  (make-subcommand name description help runs)
  subcommand?
  (name subcommand-name)
  (description subcommand-description)
  (help subcommand-help)
  (runs subcommand-runs))

(define (show-help subcommands)
  (show-version)
  (display "User-scriptable deployment tool.\n")
  (newline)
  (display "Subcommands:\n")
  (map
   (lambda (subcommand)
     (display (format #f "  ~15a  ~a\n"
                      (subcommand-name subcommand)
                      (subcommand-description subcommand))))
   subcommands))

(define (show-version)
  (format #t "userops ~a\n" userops-version-number))

(define (cli-builder subcommands tasks hosts)
  (lambda (args)
    (define (get-subcommand this-subcommand)
      (find (lambda (subcommand)
              (equal? (subcommand-name subcommand)
                      this-subcommand))
            subcommands))
    (match args
      ;; display help if first arg is "-h"
      ((progname (or "-h" "--help") rest ...)
       (show-help subcommands))
      ;; display the version if first arg is "-h"
      ((progname (or "-v" "--version") rest ...)
       (show-version))
      ;; Run the subcommand
      ((progname (= get-subcommand subcommand) rest-args ...)
       (cond
        ;; No subcommand?  Show help!
        ((not subcommand)
         (show-help subcommands))
        ;; Help on the subcommand?  Sure thing!
        ((or (member "-h" rest-args)
             (member "--help" rest-args))
         (begin (format #t "~a -- ~a\n\n~a\n"
                         (subcommand-name subcommand)
                         (subcommand-description subcommand)
                         (subcommand-help subcommand))))
        ;; Run that subcommand!
        (else
         ((subcommand-runs subcommand)
          subcommand
          rest-args
          tasks hosts))))
      ;; Otherwise else, show help!
      (_ (show-help subcommands)))))

(define (get-host-from-list host-name hosts)
  (find (lambda (host)
          (equal? (slot-ref host 'name)
                  host-name))
        hosts))

(define common-subcommands
  (list
   (make-subcommand
    "list-hosts"
    "List relevant hosts."
    "  -h --help    Help on this thing"
    (lambda (subcommand args tasks hosts)
      (display "Available hosts:\n")
      (map
       (lambda (host)
         (format #t " - ~a\n" (slot-ref host 'name)))
       hosts)))
   (make-subcommand
    "remote-command"
    "Run a shell command (not a task) on a remote host."
    "Call like:
  cli.scm remote-command HOSTNAME cat /etc/hosts
or:
  cli.scm remote-command HOSTNAME \"cat /etc/hosts\"\n"
    (lambda (subcommand args tasks hosts)
      (define (get-host host-name)
        (get-host-from-list host-name hosts))
      (match args
        ;; If the first parameter is one of our hosts...
        (((= get-host (? host? host))
          command ...)
         (run-command-remotely host command))
        ;; Not a host, but fits the pattern
        ((not-a-host command ...)
         (format #t "No such host: ~s\n" not-a-host))
        (_ (display (subcommand-help subcommand))))))))

(define (standard-cli tasks hosts)
  (cli-builder
   common-subcommands
   tasks hosts))
