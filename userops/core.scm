(define-module (userops core)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:export (<context>
            make-context
            context?
            <task>
            task?
            exec-task
            define-task
            run-tasks
            <host>
            host?))

;; ------------
;; Task context
;; ------------

(define-record-type <context>
  (make-context host extra)
  context?
  (host context-host)
  (extra context-extra))

;; ----------
;; Task stuff
;; ----------

(define-class <task> ()
  (exec-args
   #:init-keyword #:exec-args)
  (description
   #:init-keyword #:description))

(define (task? object)
  (is-a? object <task>))

;; Default task execution is a NOOP
(define-method (exec-task (task <task>) context)
  (lambda () #:nil))

(define-syntax-rule (define-task constructor class exec-func)
  "Define a task with shorthand CONSTRUCTOR and classname CLASS, with
execution function EXEC-FUNC (probably a lambda).

Simple example:

  (define-task echo-this <echo-this>
    (lambda (string-to-echo)
      (display string-to-echo)))

  (exec-task (echo-this \"testing testing\"))"
  (begin (define-class class (<task>))
         (define-method (exec-task (this-task class) context)
           (apply exec-func (cons context (slot-ref this-task 'exec-args))))
         (define (constructor . exec-args)
           ;; Initialize, set args, return class
           (let ((instance (make class)))
             (slot-set! instance 'exec-args exec-args)
             instance))))

(define (run-tasks task-list context)
  (map (lambda (task)
         (exec-task task context))
       (remove unspecified? task-list)))


;; ----------
;; Host stuff
;; ----------

(define-class <host> ()
  (name #:init-keyword #:name)
  (host #:init-keyword #:host)
  (port #:init-value 2222
        #:init-keyword #:port)
  (description #:init-keyword #:description)
  (groups #:init-form list
          #:init-keyword #:groups))

(define (host? object)
  (is-a? object <host>))

;; Hm

(define-method (display (host <host>) port)
  (format port "#<<host> ~a>" (slot-ref host 'name)))

