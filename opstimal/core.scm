;;; Opstimal -- Deployment that makes sense
;;;
;;; Copyright (C) 2015 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;; Use immutable context for task state stuff
;; Use fluids for all imperative i/o type state stuff?

(define-module (opstimal core)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu) ;; immutable records
  #:use-module (oop goops)
  #:use-module (opstimal unique-id)
  #:export (remove-keyword-arguments
            ;; task stuff
            make-task
            task?
            task-func
            task-id
            task-desc
            task-default-desc
            task-init-func
            task-args
            task-pick-desc
            ;; result stuff
            ;;   make-result is not exposed to the user, use make-result*
            ;;   ... is that strange?
            make-result*
            result?
            result-status
            result-props
            result-task
            result-context
            result-run-id
            result-prop-assq
            result-children
            ;; functions
            task-run
            task-start
            run-tasks-until-failure
            define-task
            update-result-props
            vhash-from-keyword-list
            result-status-valid?
            enforce-result-status-valid
            result-changed?
            result-failed?
            result-unchanged?
            result-skipped?
            result-completed?
            ;; Onto the statuslog...
            <statuslog-printer>
            <simple-port-printer>
            statuslog
            statuslog-printer
            statuslog-as-string
            make-string-wrapped-port
            <tracked-run>
            make-tracked-run
            tracked-run-id
            tracked-run-task
            tracked-run-wrapped-port
            tracked-run-string-port
            tracked-run-status
            printer-start-task
            printer-end-task
            statuslog-for-run
            run-tasks-gather-results
            run-all
            task-list))


(define* (remove-keyword-arguments xs #:optional these-keys)
  "Remove #:foo keyword arguments

By default, removes all keyword arguments and their values
from expression XS

If THESE-KEYS is provided, only removes keywords specifically
from that list."
  (let ((relevant-keyword?
         (if these-keys
             (lambda (x)
               (and (keyword? x)
                    (member x these-keys)))
             keyword?)))
    (if (null? xs)
      xs
      (match xs
        (((? relevant-keyword? key) value . rest)
         (remove-keyword-arguments rest these-keys))
        ((value . rest)
         (cons value (remove-keyword-arguments rest these-keys)))))))


;; In a certain sense, we're defining our own "delay" and "force"
;; here with metadata about how these were originally called.


(define-immutable-record-type <task>
  (make-task func id desc default-desc init-func args)
  task?
  (func task-func)
  ;; The id of the task upon instantiation (but not execution,
  ;;   that's the run-id in the context)
  (id task-id)
  ;; Description of the task for the statuslog, if any
  ;;   ... users can usually set this on a per-instatntiation basis
  (desc task-desc)
  ;; Default desc provided by the task
  (default-desc task-default-desc)
  ;; This is here for introspection purposes
  (init-func task-init-func)
  (args task-args))

(set-record-type-printer!
 <task>
 (lambda (task port)
   (format port
           "#<task ~a desc: ~s id: ~s>"
           (procedure-name (task-init-func task))
           (task-desc task)
           (task-id task))))

(define (task-pick-desc task)
  "Pick out which description we should probably show the user"
  (or (task-desc task) (task-default-desc task)))

(define-immutable-record-type <result>
  (make-result status props)
  result?
  (status result-status)
  (props result-props)
  ;; @@: not sure if this is a good idea or not.
  ;;  if so, it could be simply appended by task-run,
  ;;  rather than the task's function itself.
  (task result-task)
  (context result-context)
  (log result-log)
  (run-id result-run-id))


;; @@: Should there be a start-tasks?
(define* (task-start task
                     #:optional
                     (context vlist-null)
                     printer)
  "Begin running tasks starting with root TASK and a fresh PRINTER

If PRINTER is not supplied, <simple-port-printer> is used."
  (parameterize
      ((statuslog-printer (or printer (make <simple-port-printer>))))
    (task-run task context)))


(define* (task-run task #:optional (context vlist-null))
  "Run a TASK using CONTEXT

If kicking off a fresh set of tasks, maybe use task-start."
  (let* ((parent-id (cdr (or (vhash-assq 'run-id context)
                             ;; annoying, we just want an #f on the cdr
                             ;; if 'run-id not set
                             '(#f . #f))))
         (run-id (unique-id))
         (new-context   ;; update context w/ a unique "run-id"
          (vhash-cons 'run-id run-id context)))
    (printer-start-task (statuslog-printer) run-id parent-id task)
    (parameterize ((statuslog
                    (statuslog-for-run (statuslog-printer) run-id)))
      (let ((result
             ;; Actually run the function!
             ((task-func task) new-context)))
        (if (and (result? result) (enforce-result-status-valid result))
            ;; Return the result, but with information on the original task
            ;; and the context it was run with
            (let ((final-result
                   (set-fields result
                               ((result-log)
                                (statuslog-as-string
                                 (statuslog-printer)
                                 run-id))
                               ((result-task) task)
                               ;; Note that we append the initial context,
                               ;; not the verison with the run-id, for
                               ;; reproducibility
                               ((result-context) context)
                               ;; We still have the task-id anyway
                               ((result-run-id) run-id))))
              (printer-end-task (statuslog-printer) run-id final-result)
              final-result)
            ;; If there was no valid result returned from task,
            ;; we should throw an error
            (error 'no-result-returned-from-task))))))


(define (run-tasks-until-failure tasks context)
  "Run all tasks in TASKS with CONTEXT until we hit a failure"
  (if (null? tasks)
      '()
      (let ((result
             (task-run (car tasks) context)))
        (if (result-failed? result)
            ;; stop now
            (list result)
            ;; otherwise else keep going
            (cons result (run-tasks-until-failure (cdr tasks) context))))))


(define-syntax define-task
  (lambda (x)
    "Define a task"
    (syntax-case x ()
      ((_ (task-name . args) doc task-lambda . init-kwargs)
       (string? (syntax->datum #'doc))
       #'(define* (task-name #:key desc
                             #:allow-other-keys
                             #:rest rest)
           doc
           (let ((clean-args
                  (remove-keyword-arguments rest '(#:desc)))
                 (builder
                  (lambda* (. args)
                    task-lambda)))
             (let-keywords
              (list . init-kwargs) #f (default-desc)
              (make-task
               (apply builder clean-args)
               (unique-id)
               desc
               (if default-desc
                   (apply default-desc clean-args)
                   (format #f "Running ~s"
                           (symbol->string (quote task-name))))
               ;; This is here for introspection purposes
               task-name
               rest)))))
      ;; support w/o docstrings
      ((_ (task-name . args) task-lambda . init-kwargs)
       #'(define-task (task-name . args) "" task-lambda . init-kwargs)))))


(define (make-result* status . props)
  "Make a result object with STATUS any key-value pairs from PROPS

Status should be one of '(unchanged changed failed skipped)"
  (let ((result
         (make-result status
                      (vhash-from-keyword-list
                       props))))
    ;; @@: Should we move this to make-result?
    (enforce-result-status-valid result)
    result))


(define (update-result-props result . props)
  "Update RESULT object's PROPS"
  (set-field result
             (result-props)
             (vhash-from-keyword-list props (result-props result))))


(define* (vhash-from-keyword-list new-props
                                  #:optional (current-props vlist-null))
  "Generate a vhash from a keyword list

You can also use this to update an existing vhash using current-props,
which is how this function recursively expands anyway"
  (match new-props
    (((? keyword? keyword) value . rest)
     (vhash-from-keyword-list
      rest
      (vhash-cons keyword
                  value current-props)))
    ((? null? _) current-props)
    ;; Do we need this explicit throw?
    (_ (throw 'malformed-keyword-list))))


(define (result-status-valid? result)
  "Check if RESULT's status is valid

Valid result statuses are: (unchanged changed failed skipped)"
  (member (result-status result)
          '(unchanged changed failed skipped)))


(define (enforce-result-status-valid result)
  "Raises 'invalid-result-status if invalid result status"
  (if (not (result-status-valid? result))
      (error 'invalid-result-status)))


(define (result-status-check-factory expected-status)
  (lambda (result)
    (enforce-result-status-valid result)
    (eq? (result-status result) expected-status)))
 

(define result-changed? (result-status-check-factory 'changed))
(define result-failed? (result-status-check-factory 'failed))
(define result-unchanged? (result-status-check-factory 'unchanged))
(define result-skipped? (result-status-check-factory 'skipped))

(define (result-completed? result)
  "Completed is defined as not failed"
  (not (result-failed? result)))

(define (result-prop-assq result prop)
  "Get the value of PROP from RESULT"
  (cdr (vhash-assq prop (result-props result))))

(define (result-children result)
  "Get a list of all children of this property, if any"
  (or (result-prop-assq result #:children) '()))


;; ----------------------------------------------------------------------
;; Statuslog base class
;; ----------------------------------------------------------------------

;; Okay, want to implement your own statuslog?
;; The statuslog is set up in such a way where you can extend
;; some methods via define-method with your own class and do something
;; pretty different.  For now, what you need to implement is:
;; 
;;  - printer-start-task
;;  - printer-end-task
;;  - statuslog-for-run
;;  - statuslog-as-string
;;
;; From here on out you could make your printer/statuslog do something
;; pretty different.  The statuslog/printer may seem a bit complex,
;; but by using parameters in this way and allowing the user to switch
;; out the statuslog, you could do something cool like make a printer
;; which hooks into a web application, maybe pushing updates via
;; websockets, and push updates from each task as you go.
;;
;; It's a little bit tricky to implement a statuslog & printer,
;; but luckily the interface is kept pretty easy for task writers,
;; since they're just writing to a port.


;; Task run we're tracking for the statuslog
(define-record-type <tracked-run>
  (make-tracked-run
   id
   parent-id
   task wrapped-port string-port
   ;; @@: should we track the status or the result?
   status
   task-level)
  tracked-run?
  (id tracked-run-parent-id)
  (parent-id tracked-parent-id)
  ;; Should we put parent-id here?
  (task tracked-run-task)
  (wrapped-port tracked-run-wrapped-port)
  (string-port tracked-run-string-port)
  (status tracked-run-status set-tracked-run-status!)
  (task-level tracked-run-task-level))


;; @@: Eventually we may refactor stuff from the <simple-port-printer>
;;   into here
(define-class <statuslog-printer> ())

(define-class <simple-port-printer> (<statuslog-printer>)
  ;; Tasks that we're done recording info about
  (tracked-runs #:init-thunk make-hash-table)
  ;; This is the output port that we actually dump all our data to
  (output-port #:init-thunk current-output-port
               #:init-keyword #:output-port))

;; This one not needed by all printers, but it is by <simple-port-printer>
;; at least..
(define-method (printer-tracked-run-task-level
                printer run-id)
  (let ((tracked-run
         (hash-ref (slot-ref printer 'tracked-runs)
                   run-id)))
    (if tracked-run
        (tracked-run-task-level tracked-run)
        0)))

;; Printer methods, the generic ones


;; @@: Do we want to pass in a run-id, or a context that has a run-id set?
;;   I guess we can always add that later...
(define-method (printer-start-task (printer <simple-port-printer>)
                                   run-id parent-id task)
  ;; @@: should we also track the parent run id?  is that possible?
  ;; ... maybe if setting a parent-id in the context, but that
  ;;     would mean passing one's own id into run-tasks, which seems
  ;;     like maybe too much work for many tasks?  I don't know...
  ;;
  ;;     in such a change to run-tasks, we could keep the parent-id as
  ;;     whatever the current context run-id is.  Hackily though, we could also
  ;;     just extract that id by seeking the second definition of run-id
  ;;     in the vhash.  *definitely* hacky!
  (let* ((output-port (slot-ref printer 'output-port))
         (parent-level (printer-tracked-run-task-level printer parent-id))
         (task-level (+ parent-level 1)))
    (receive (wrapped-port string-port)
        (make-string-wrapped-port (slot-ref printer 'output-port))
      (hash-set! (slot-ref printer 'tracked-runs)
                 run-id
                 (make-tracked-run run-id parent-id task
                                   wrapped-port string-port
                                   'running
                                   task-level)))
    (format output-port "~a> ~a\n" (make-string task-level #\=) (task-pick-desc task))))

(define-method (printer-end-task (printer <simple-port-printer>) run-id result)
  (let ((output-port (slot-ref printer 'output-port))
        (tracked-run (hash-ref (slot-ref printer 'tracked-runs)
                               run-id)))
    (format output-port "<~a [~a] ~a\n"
            (make-string (tracked-run-task-level tracked-run) #\=)
            (string-upcase (symbol->string (result-status result)))
            (task-pick-desc (tracked-run-task
                             tracked-run)))
    (set-tracked-run-status!
     tracked-run
     (result-status result))))

(define-method (statuslog-for-run (printer <simple-port-printer>) run-id)
  (tracked-run-wrapped-port (hash-ref (slot-ref printer 'tracked-runs) run-id)))

(define-method (statuslog-as-string (printer <simple-port-printer>) run-id)
  (get-output-string
   (tracked-run-string-port
    (hash-ref (slot-ref printer 'tracked-runs) run-id))))


;; ----------------------------------------------------------------------
;; simple printer
;; ----------------------------------------------------------------------

(define statuslog-printer (make-parameter (make <simple-port-printer>)))
(define statuslog (make-parameter (current-output-port)))

(define* (make-string-wrapped-port #:optional existing-port)
  "Take a port and wrap it in a string port that writes to both

If existing-port unspecified, use (current-output-port)

Returns multiple values: wrapped-port and string-port"
  (let* ((port-to-wrap (or existing-port (current-output-port)))
         (string-port (open-output-string)))
    (values
     (make-soft-port
      (vector
       (lambda (c)
         (write c port-to-wrap)
         (write c string-port))
       (lambda (s)
         (display s port-to-wrap)
         (display s string-port))
       (lambda ()
         (force-output port-to-wrap)
         (force-output string-port))
       #f
       ;; @@: Do we ever really want to close the port here?
       ;;   That seems like not our job.
       (lambda ()
         #nil))
      "w")
     string-port)))


;; ---------------------------------------
;; Core tasks, used pretty much everywhere
;; ---------------------------------------

(define (run-tasks-gather-results context tasks)
  (define (result-unchanged-or-skipped? x)
    (or (result-skipped? x)
        (result-unchanged? x)))

  (let ((results (run-tasks-until-failure tasks context)))
    (match results
      ((_ ... (? result-failed? last-one-failed))
       (make-result* 'failed
                     #:children
                     results))
      ;; due to a hack, this captures empty result lists too ;)
      (((? result-skipped? skipped-task) ...)
       (make-result* 'skipped
                     #:children
                     results))
      (((? result-unchanged-or-skipped?
           unchanged-task) ...)
       (make-result* 'unchanged
                     #:children
                     results))
      (_
       (make-result* 'changed
                     #:children
                     results)))))

(define-task (run-all tasks)
  "Run all tasks in TASKS

Will bail out early if a task fails."
  (lambda (context)
    (run-tasks-gather-results context tasks)))

(define-task (task-list . tasks)
  "Run all tasks, as positional arguments"
  (lambda (context)
    (run-tasks-gather-results context tasks)))


;; ---- testing things ----

(define-task (apt-get-install package)
  "<mock> apt-get install PACKAGE"
  (lambda (context)
    (display (string-append "Installing " package "!\n") (statuslog))
    (make-result* 'changed))
  #:default-desc
  (lambda (package)
    (format #f "We're gonna be installing ~s!" package)))

(define-task (apt-get-update)
  "<mock> run apt-get update"
  (lambda (context)
    (display "updating apt" (statuslog))
    (newline)
    (make-result* 'changed)))



(define-task (copy-file-task src dest #:key owner group)
  "Copy a file from SRC to DEST, possibly setting OWNER and GROUP."
  (lambda (context)
    (format #f "Copying ~s to ~s\n" src dest)
    (if owner
        (format #f " - setting owner to ~s\n" owner))
    (if group
        (format #f " - setting group to ~s\n" group))))

(define-task (notify-if-changed #:key notify . tasks)
  (lambda (context)
    (let ((cleaned-tasks
           (remove-keyword-arguments tasks
                                     '(#:notify))))
      (task-run (run-all tasks)))))
