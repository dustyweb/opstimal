;;; Opstimal -- Deployment that makes sense
;;;
;;; Copyright (C) 2015 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;; convert to userops-local parameter

(define-module (opstimal unique-id)
  #:use-module (ice-9 format)
  #:export (unique-id
            *unique-id-random-state*))

(define *unique-id-random-state* (make-parameter (random-state-from-platform)))

(define (unique-id)
  "Generate and return a unique id"
  ;; Not really a formal method, but a larger number than a uuid4
  (format #f "~x" (random (expt 10 50) (*unique-id-random-state*))))

