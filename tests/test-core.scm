;;; Opstimal -- Deployment that makes sense
;;;
;;; Copyright (C) 2015 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; The purpose


#!/usr/bin/guile \
-s
!#

(define-module (tests test-core)
  #:use-module (opstimal core)
  #:use-module (srfi srfi-64)
  #:use-module (oop goops)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 receive)
  #:use-module (tests utils))

(test-begin "test-core")

(test-equal
 '(#:keeper yeah #:dont-remove yes)
 (remove-keyword-arguments
  '(#:remove-me aww #:keeper yeah #:dont-remove yes #:also-remove-this fine)
  '(#:remove-me #:also-remove-this)))


(define-task (apt-get-update)
  "<mock> run apt-get update"
  (lambda (context)
    (display "updating apt\n" (statuslog))
    (newline)
    (make-result* 'changed)))

(define-task (apt-get-install package)
  "<mock> apt-get install PACKAGE"
  (lambda (context)
    (display (string-append "Installing " package "!\n") (statuslog))
    (make-result* 'changed))
  #:default-desc
  (lambda (package)
    (format #f "We're gonna be installing ~s!" package)))


;; Make sure that a fresh task returns a task-id, which
;;  should be a string (similar with the run-id on the result)
;;  ... probably a nicer test here would be good, if we could figure
;;      out how to reliably reproduce a random seed

(test-assert
 (string? (task-id (apt-get-update))))

(test-assert
 (string? (result-run-id (task-run (apt-get-update)))))

;; make sure that multiple runs of the same task don't have the same id

(let* ((ready-to-go-task (apt-get-update))
       (run-id-1 (result-run-id (task-run ready-to-go-task)))
       (run-id-2 (result-run-id (task-run ready-to-go-task))))
  (test-assert
   (not (equal? run-id-1 run-id-2))))


;; Test description stuff
;; ----------------------

;; apt-get-update has no default
(test-equal (task-desc (apt-get-update)) #f)
(test-equal (task-desc (apt-get-update #:desc "We're gonna apt-get update now"))
            "We're gonna apt-get update now")
(test-equal (task-default-desc (apt-get-update))
            "Running \"apt-get-update\"")
(test-equal (task-default-desc (apt-get-update #:desc "We're gonna apt-get update now"))
            "Running \"apt-get-update\"")
(test-equal (task-pick-desc (apt-get-update))
            "Running \"apt-get-update\"")
(test-equal (task-pick-desc (apt-get-update #:desc "We're gonna apt-get update now"))
            "We're gonna apt-get update now")

;; apt-get-install has a default
(test-equal (task-desc (apt-get-install "emacs")) #f)
(test-equal (task-desc (apt-get-update
                        #:desc "We're gonna apt-get and install emacs now"))
            "We're gonna apt-get and install emacs now")
(test-equal (task-default-desc (apt-get-install "emacs"))
            "We're gonna be installing \"emacs\"!")
(test-equal (task-default-desc (apt-get-install "emacs"
                                #:desc "We're gonna apt-get and install emacs now"))
            "We're gonna be installing \"emacs\"!")
(test-equal (task-pick-desc (apt-get-install "emacs"))
            "We're gonna be installing \"emacs\"!")
(test-equal (task-pick-desc (apt-get-install "emacs"
                             #:desc "We're gonna apt-get and install emacs now"))
            "We're gonna apt-get and install emacs now")


;; -------------------------------------------------------
;; Test that tasks execute in their simplest form
;; -------------------------------------------------------

(define check-me #nil)

(define-task (set-check-me-to! val)
  "set the check-me var to something"
  (lambda (context)
    (set! check-me val)
    (make-result* 'changed)))

(begin
  (set! check-me "nothing")  ;; make re-evaluating this test in a repl easier ;)
  (task-run (set-check-me-to! "something"))
  (test-equal check-me "something"))


;; ---------------
;; statuslog tests
;; ---------------

(define test-output "")

(call-with-output-string
 (lambda (test-output-string)
   (receive (wrapped-port string-port)
       (make-string-wrapped-port test-output-string)
     (display "test lol" wrapped-port)
     (newline wrapped-port)
     (display "seriously lol" wrapped-port)
     (test-equal (get-output-string string-port)
                 "test lol\nseriously lol"))
   (set! test-output test-output-string)
   (test-equal (get-output-string test-output-string)
               "test lol\nseriously lol")))


(test-equal
 (call-with-output-string
  (lambda (test-output-string)
    (let* ((printer
            (make <simple-port-printer>
              #:output-port test-output-string))
           (result
            (task-start
             (run-all (list (apt-get-update)
                            (apt-get-install "linooks")))
             vlist-null
             printer)))
      result
      )))
 "=> Running \"run-all\"
==> Running \"apt-get-update\"
updating apt
<== [CHANGED] Running \"apt-get-update\"
==> We're gonna be installing \"linooks\"!
Installing linooks!
<== [CHANGED] We're gonna be installing \"linooks\"!
<= [CHANGED] Running \"run-all\"
")


;; Test result-children and result-prop-assq

(define-task (set-testy-result-prop-to val)
  (lambda (context)
    (make-result* 'changed #:testy val)))

(test-equal
 (map
  (lambda (x)
    (result-prop-assq x #:testy))
  (result-children
   (task-start
    (task-list (set-testy-result-prop-to "one")
               (set-testy-result-prop-to "two")))))
 '("one" "two"))

(test-end "test-core")

(test-exit)
